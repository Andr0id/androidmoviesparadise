package ui_tests;

import android.view.View;

import com.halil.ozel.movieparadise.R;
import com.halil.ozel.movieparadise.ui.main.MainActivity;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.filters.LargeTest;
import androidx.test.runner.AndroidJUnit4;

import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MenuItemsTest {
    // TODO: implement Page Object architecture and replace all these variables into MainScreen class
    private Matcher<View> menuNowPlaying = allOf(
            withText("Now Playing"),
            ViewMatchers.withId(R.id.row_header),
            ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
    );

    private Matcher<View> menuTopRated = allOf(
            withText("Top Rated"),
            ViewMatchers.withId(R.id.row_header),
            ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
    );

    private Matcher<View> menuPopular = allOf(
            withText("Popular"),
            ViewMatchers.withId(R.id.row_header),
            ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
    );

    private Matcher<View> menuUpcoming = allOf(
            withText("Upcoming"),
            ViewMatchers.withId(R.id.row_header),
            ViewMatchers.withEffectiveVisibility(ViewMatchers.Visibility.VISIBLE)
    );

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void check_whether_menu_item_now_playing_is_displayed() throws InterruptedException {
        Thread.sleep(1500); // TODO: replace it with idling
        Espresso.onView(menuNowPlaying).check(matches(isDisplayed()));
    }

    @Test
    public void check_whether_menu_item_top_rated_is_displayed() throws InterruptedException {
        Thread.sleep(1500); // TODO: replace it with idling
        Espresso.onView(menuTopRated).check(matches(isDisplayed()));
    }

    @Test
    public void check_whether_menu_item_popular_is_displayed() throws InterruptedException {
        Thread.sleep(1500); // TODO: replace it with idling
        Espresso.onView(menuPopular).check(matches(isDisplayed()));
    }

    @Test
    public void check_whether_menu_item_upcoming_is_displayed() throws InterruptedException {
        Thread.sleep(1500); // TODO: replace it with idling
        Espresso.onView(menuUpcoming).check(matches(isDisplayed()));
    }
}
